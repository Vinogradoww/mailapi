from passlib.context import CryptContext
from sqlalchemy.orm import Session
from datetime import datetime
import model


def get_password_hash(password):
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    return pwd_context.hash(password)


def add_user(db: Session, login = "admin", password = "admin", mail_from = '', name_mail_from = ''):
    hashed_password = get_password_hash(password)
    login_exists = db.query(model.User).filter(model.User.login == login).first()
    password_exists = db.query(model.User).filter(model.User.hashed_password == hashed_password).first()
    mail_from_exist = db.query(model.MailFrom).filter(model.MailFrom.mail_from == mail_from).first()
    if not login_exists:
        if not password_exists:
            if mail_from:
                mail_from_exist = db.query(model.MailFrom).filter(model.MailFrom.mail_from == mail_from).first()
                if mail_from_exist:
                    mf = db.query(model.MailFrom).filter(model.MailFrom.mail_from == mail_from).first()
                    user = model.User(login = login, hashed_password = hashed_password, mail_from_id = mf.id)
                else:
                    new_mf = add_mail_from(db, mail_from, name_mail_from)
                    user = model.User(login = login, hashed_password = hashed_password, mail_from_id = new_mf.id)
                    db.commit()
            else:
                mf = db.query(model.MailFrom).first()
                user = model.User(login = login, hashed_password = hashed_password, mail_from_id = mf.id)
            db.add(user)
            db.commit()
            return user
        else:
            return "password_exists"
    else:
        return "login_exists"


def get_user(db: Session, login):
    user = db.query(model.User).filter(model.User.login == login).first()
    if user:
        return {"id": user.id, "username": login, "hashed_password": user.hashed_password, "disabled": user.disabled, "mail_from_id": user.mail_from_id}


def change_password(db: Session, id, new_password):
    hashed_password = get_password_hash(new_password)
    user = db.query(model.User).filter(model.User.id == id).first()
    if user:
        password_exists = db.query(model.User).filter(model.User.hashed_password == hashed_password).first()
        if not password_exists:
            user.hashed_password = hashed_password
            db.commit()
            return user
        else:
            return "password_exists"
    #else:
        #return f"Пользователь {}"


def disable(db: Session, login):
    user = db.query(model.User).filter(model.User.login == login).first()
    if user:
        user.disabled = 1
        db.commit()
        return 1
    else:
        return 0


def enable(db: Session, login):
    user = db.query(model.User).filter(model.User.login == login).first()
    if user:
        user.disabled = 0
        db.commit()
        return 1
    else:
        return 0


def del_user(db: Session, login):
    user = db.query(model.User).filter(model.User.login == login).delete()
    db.commit()
    return user


def add_email(db: Session, recipients, _from_user_id = 1, _subject = None, _body = None, files = None, _status_id = None, _status_text = None):
    to = ''
    for r in recipients:
        to += r.decode() + ','
    path = ''
    if files:
        for f in files:
            path += f + ','
    email = model.Email(
        from_user_id = _from_user_id,
        recipients = to,
        subject = _subject.decode(),
        body = _body.decode(),
        date = datetime.now(),
        files = path, 
        status_id = _status_id,
        status_text = _status_text.decode()
    )
    db.add(email)
    db.commit()
    return email


def get_email_id(db: Session):
    emails = db.query(model.Email).all()
    last_status = emails[-1].status_id
    return len(emails) + 1


def get_email_status(db: Session, msg_id):
    email = db.query(model.Email).filter(model.Email.status_id == msg_id).first()
    if email:
        return email.status_text#eval(email.status_text)
    else:
        return email


def get_all_emails(db: Session):
    emails = db.query(model.Email).all()
    return {'data': [{"id": email.id,
                    "userId": email.from_user_id,
                    "recipients": email.recipients,
                    "subject": email.subject,
                    "body": email.body,
                    "date": str(email.date),
                    "files": email.files,
                    "status_id": email.status_id,
                    "status_text": email.status_text} for email in emails],
            'total': len(emails)}


def get_all_users(db: Session):
    users = db.query(model.User).all()
    return {'data': [{'id': user.id, 'login':user.login} for user in users], 'total': len(users)}


def add_mail_from(db: Session, mail_from, name_mail_from = ''):
    is_exist = db.query(model.MailFrom).filter(model.MailFrom.mail_from == mail_from).first()
    if not is_exist:
        mail_from = model.MailFrom(mail_from = mail_from, name_mail_from = name_mail_from)
        db.add(mail_from)
        db.commit()
        return mail_from
    else: 
        return is_exist

def change_mail_from(db: Session, login, mail_from, name_mail_from = ''):
    user = db.query(model.User).filter(model.User.login == login).first()
    if user:
        mail_from_query = db.query(model.MailFrom).filter(model.MailFrom.mail_from == mail_from).first()
        if mail_from_query:
            user.mail_from_id = mail_from_query.id
            db.commit()
        else:
            new_mail_from = add_mail_from(db, mail_from, name_mail_from)
            user.mail_from_id = new_mail_from.id
            db.commit()
        return user
    else:
        return f"Пользователь {login} не найден"

def change_name_mail_from(db: Session, mail_from, new_name_mail_from):
    mail_from = db.query(model.MailFrom).filter(model.MailFrom.mail_from == mail_from).first()
    if mail_from:
        mail_from.name_mail_from = new_name_mail_from
        db.commit()
    else:
        mail_from = f"{mail_from} не найден"
    return mail_from

def del_mail_from(db: Session, mail_from):
    mail_from_query = db.query(model.MailFrom).filter(model.MailFrom.mail_from == mail_from).first()
    usrs = db.query(model.User).filter(model.User.mail_from_id == mail_from_query.id).all()
    if usrs:
        res_usrs = ''
        default_m_f = db.query(model.MailFrom).first()
        for u in usrs:
            res_usrs += u.login + ', '
            u.mail_from_id = default_m_f.id
        db.query(model.MailFrom).filter(model.MailFrom.mail_from == mail_from).delete()
        db.commit()
        return {"usrs": res_usrs[:-2], "default": default_m_f.mail_from}
    else:
        db.query(model.MailFrom).filter(model.MailFrom.mail_from == mail_from).delete()
        db.commit()
        return 1


def get_mail_from(db: Session, id = 1):
    user = db.query(model.User).filter(model.User.id == id).first()
    mail_from = db.query(model.MailFrom).filter(model.MailFrom.id == user.mail_from_id).first()
    return mail_from.mail_from, mail_from.name_mail_from


def is_admin(db: Session, login):
    user = db.query(model.User).filter(model.User.login == login).first()
    return user.admin


def add_to_admins(db: Session, login):
    user = db.query(model.User).filter(model.User.login == login).first()
    if user:
        user.admin = 1
        db.commit()
        return user
    else: 
        return None

if __name__ == '__main__':
    from database import SessionLocal
    #get_sender(SessionLocal, 1)
    #add_sender(SessionLocal, "sender@test.ru")
    del_user(SessionLocal, 'test3')
    #ans, name = get_mail_from(SessionLocal, 25)
    #print(ans, name)
    #get_last_email_id(SessionLocal)
    #print(get_all_users(SessionLocal))
    #add_user(SessionLocal)
    #print(get_user(SessionLocal, 'admin'))
    #emails = SessionLocal.query(model.Email).first()
    #print(emails.recipients)

       




