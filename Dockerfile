FROM python:latest
ADD main.py auth.py database.py model.py schemas.py updatedb.py delivery_status.py testTEST.py /
RUN mkdir static 
RUN mkdir templates
COPY static /static
COPY templates /templates
ADD requirements.txt /
RUN pip install -r requirements.txt
CMD ["python", "main.py"]
