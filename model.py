from sqlalchemy import Column, Integer, Boolean, String, DateTime, Float, Text, ForeignKey
from sqlalchemy.orm import relationship
from database import ENGINE
from database import Base



class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, autoincrement=True)
    login = Column(String(255), nullable=False)
    hashed_password = Column(String(255), nullable=False)
    disabled = Column(Boolean, default = False)
    mail_from_id = Column(Integer, ForeignKey("mail_from.id"))
    admin = Column(Boolean, nullable=False, default = 0)
    children = relationship("Email", cascade="all,delete", backref="user")

class Email(Base):
    __tablename__ = 'email'
    id = Column(Integer, primary_key=True, autoincrement=True)
    from_user_id = Column(Integer, ForeignKey("user.id"))
    recipients = Column(Text)
    subject = Column(Text)
    body = Column(Text)
    date = Column(DateTime)
    files = Column(Text, nullable=False)
    status_id = Column(Integer)
    status_text = Column(Text)

class MailFrom(Base):
    __tablename__ = 'mail_from'
    id = Column(Integer, primary_key=True, autoincrement=True)
    mail_from = Column(String(45), nullable=False)
    name_mail_from = Column(String(225))
        

def main():
    Base.metadata.create_all(bind=ENGINE)

if __name__ == "__main__":
    main()
