from fastapi import FastAPI, BackgroundTasks, Depends, status, HTTPException, Form
#from fastapi_mail import FastMail, MessageSchema, ConnectionConfig
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
import uvicorn

from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
import smtplib

from pydantic import EmailStr, BaseSettings
from starlette.responses import JSONResponse, FileResponse

from starlette.requests import Request
from cryptography.fernet import Fernet
from copy import deepcopy
from pathlib import Path
from loguru import logger
import logging
import shutil
import os

from database import SessionLocal
import model
import updatedb
import auth
import schemas
import delivery_status


class InterceptHandler(logging.Handler):
    def emit(self, record):
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1
        logger.opt(depth=depth, exception=record.exc_info).log(
            level, record.getMessage()
        )
console_out = logging.StreamHandler()

logging.getLogger().handlers = [InterceptHandler(), console_out]
logger.configure(
    handlers=[{"sink": "log/log.log", "level": logging.DEBUG}]
)
logging.getLogger("uvicorn.access").handlers = [InterceptHandler(), console_out]



if __name__ == "__main__":
    uvicorn.run("main:app", host = "0.0.0.0", port=5001, log_level="info")
app = FastAPI(title="main mail")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Settings(BaseSettings):
    MAIL_PORT = os.environ["MAIL_PORT"]
    MAIL_SERVER = os.environ["MAIL_SERVER"]
    CRYPTO_KEY = os.environ["CRYPTO_KEY"]
    SRC_IP = os.environ["SRC_IP"]

settings = Settings()



@app.post("/token", response_model = schemas.Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = auth.authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "Incorrect username or password",
            headers = {"WWW-Authenticate": "Bearer"},
        )
    elif type(user) == str:
        raise HTTPException(status_code = 403, detail = "User disabled")
    access_token = auth.create_access_token(data = {"sub": user.username})
    return {"access_token": access_token, "token_type": "bearer"}



@app.get("/users/me/", response_model = schemas.User)
async def read_users_me(current_user: schemas.User = Depends(auth.get_current_active_user)):
    return current_user



def crypto(string, do = 'encode'):
    fernet = Fernet(settings.CRYPTO_KEY.encode("utf-8")) 
    if type(string) == str:
        string = string.encode('utf-8')
    if do == "encode":
        return fernet.encrypt(string)
    elif do == "decode":
        return fernet.decrypt(string).decode()

if not os.path.exists('attachments/'):
    os.mkdir('attachments/')

def save_attachments(files):
    for f in files:
        attachments = Path() / 'attachments' / f.filename
        with attachments.open("wb") as buffer:
            shutil.copyfileobj(f.file, buffer)

def check_unsubscribers(recipients, force):
    if type(recipients) == str:
            recipients = recipients.split(",")
    unsubscribers = []

    if os.path.exists("unsubscribers/unsubscribers.txt"):
        with open("unsubscribers/unsubscribers.txt", "r") as f:
            unsubscribers = f.read().split(";")
    
    if force == 1:
        unsubscribers = []
    elif type(force) == list:
        tmp = []
        for u in unsubscribers:
            if u not in force:
                tmp.append(u)
        unsubscribers = tmp
    res = []
    res_unsubscribers=[]
    for r in recipients:
        if r not in unsubscribers:
            res.append(r)
        else:
            res_unsubscribers.append(r)
    return res, res_unsubscribers
#--------------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------send_email-----------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------
async def send_email(data, current_user, msg_id, files, copy_files, force_delivery):
    logger.info(f"current_user: {current_user}")
    try:
        if files:
            save_attachments(copy_files)
        recipients, unsubscribers = check_unsubscribers(recipients = data["recipients"], force = force_delivery)
        session = SessionLocal()
        mail_from, name_mail_from = updatedb.get_mail_from(session, id = current_user.id)
        session.close()
        '''conf = ConnectionConfig(
            MAIL_USERNAME = "none",
            MAIL_PASSWORD = "none",
            MAIL_FROM = mail_from,
            MAIL_PORT = int(settings.MAIL_PORT),
            MAIL_SERVER = settings.MAIL_SERVER,
            MAIL_FROM_NAME = name_mail_from,#"Никита".encode("idna"),#"Nikita",
            MAIL_TLS = False,
            MAIL_SSL = False,
            USE_CREDENTIALS = False,
        )'''
        if recipients:
            for r in recipients:
                #--------------------------------------email-----------------------------------------
                '''message = MessageSchema(
                    subject = data["subject"],
                    recipients = [r],
                    html = f"""{data["body"]}\
                    <div>\
                        ------------------------------------------------------------\
                        <p><a style='color: #ccc; font-size: 10px; text-decoration: none;' href='http://{settings.SRC_IP}/unsubscribe_me?email={str(crypto(r)).strip("b'")}'>Отписаться от рассылки</a></p>\
                    </div>""",
                    attachments = files
                )

                fm = FastMail(conf)
                await fm.send_message(message)'''
                msg = MIMEMultipart()
                msg['Subject'] = data["subject"]
                msg['From'] = formataddr((str(Header(name_mail_from, 'utf-8')), mail_from))
                msg['To'] = r   
                message = f"""{data["body"]}\
                    <div>\
                        ------------------------------------------------------------\
                        <p><a style='color: #ccc; font-size: 10px; text-decoration: none;' href='https://{settings.SRC_IP}/unsubscribe_me?email={str(crypto(r)).strip("b'")}'>Отписаться от рассылки</a></p>\
                    </div>""" 
                msg.attach(MIMEText(message, 'html'))

                for f in files:
                    part = MIMEApplication(f.file.read(), Name = f.filename)
                    part['Content-Disposition'] = f'attachment; filename="{f.filename}"'
                    msg.attach(part)

                server = smtplib.SMTP(settings.MAIL_SERVER, int(settings.MAIL_PORT))
                server.sendmail(msg['From'], msg['To'], msg.as_string())
                server.quit()
                #------------------------------------------------------------------------------------
            try:
                codes = delivery_status.main(recipients, msg_id, unsubscribers) # статус доставки 
            except Exception as exc:
                logger.exception(exc)
                codes = {"message": "Unexpected error"}

            #---------------------------DB---------------------------------
            session = SessionLocal()
            email = updatedb.add_email(
                session, 
                recipients = [crypto(r) for r in recipients],
                _from_user_id = current_user.id,
                _subject = crypto(data["subject"]),
                _body = crypto(data["body"]), 
                files = [f"attachments/{f.filename}" for f in files],
                _status_id = msg_id,
                _status_text = crypto(str(codes))
            )
            session.close()
            #--------------------------------------------------------------
            logger.success(f"email (id {email.id}, status_id {msg_id}) has been sent")
            return 1
        else:
            #---------------------------DB---------------------------------
            session = SessionLocal()
            email = updatedb.add_email(
                session, 
                recipients = [crypto(r) for r in recipients],
                _from_user_id = current_user.id,
                _subject = crypto(data["subject"]),
                _body = crypto(data["body"]), 
                files = [f"attachments/{f.filename}" for f in files],
                _status_id = msg_id,
                _status_text = crypto(str({"status": "Не доставлено", "message": "Все получатели отписались от рассылки"}))
            )
            session.close()
            #--------------------------------------------------------------
    except Exception as exc:
        logger.exception(exc)
        return exc
#--------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------
def check_data(data):
    if (not data) or (type(data) != dict):
        raise HTTPException(status_code = 400, detail = "invalid data")
    
    count = 3
    force = 0
    possible_keys = ["subject", "recipients", "body"]
    if "force" in data.keys(): 
        count = 4
        possible_keys.append("force")
        if data["force"] == "True":
            force = 1
        else:
            force = data["force"].split(",")

    data_keys = list(data.keys())[:count]
    for k in data_keys:
        if k not in possible_keys:
            logger.exception("Request must include 3 required keys: 'subject', 'recipients', 'body'; and one optional key: 'force'")
            #raise HTTPException(status_code = 400, detail = "Запрос должен включать в себя 3 обязательных ключа: subject, recipients, body; и один необязательный: force")
    
    files = []
    copy_files = []
    if len(list(data.keys())) > count:
        for f in list(data.keys())[count:]:
            files.append(data[f])
        for f in files:
            try:
                copy_files.append(deepcopy(f))
            except Exception as exc:
                logger.exception(f"Can not save file {f.filename}; Error: {exc}")
                #raise HTTPException(status_code = 400, detail = f"invalid data, check file {f.filename} Error: {exc}")
    
    return files, copy_files, force



@app.post("/email")
async def send_mail(
    background_tasks: BackgroundTasks,
    req: Request,
    current_user: schemas.User = Depends(auth.get_current_active_user)) -> JSONResponse:
    try:
        data = (await req.form())._dict
    except Exception as exc:
        logger.exception(exc)
        raise HTTPException(status_code = 400, detail = f"invalid data, check data or content-type Error: {exc}")

    files, copy_files, force = check_data(data) 

    session = SessionLocal()
    msg_id = updatedb.get_email_id(session)
    session.close()

    background_tasks.add_task(send_email, data, current_user, msg_id, files, copy_files, force)
    return JSONResponse(status_code = 200, content = {"id": msg_id})



@app.post("/get_status")
def check_token(id: str, current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    status_text = updatedb.get_email_status(session, id)
    session.close()
    if status_text:
        status_text = eval(crypto(status_text, "decode"))
        return JSONResponse(status_code = 200, content = status_text)
    else:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "id does not exist",
            headers = {"WWW-Authenticate": "Bearer"},
        )

#----------------------------------------------------------------admin----------------------------------------------------------------

@app.post("/add_to_admins")
async def add_to_admins(login: str = Form(...), current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        user = updatedb.add_to_admins(session, login)
        session.close()
        if user:
            return JSONResponse(status_code = 200, content = f"Пользователь {login} полчил права администратора")
        else:
            return JSONResponse(status_code = 404, content = f"Пользователь {login} не найден")
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователя admin")



@app.get("/users")
async def get_all_users(current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        users = updatedb.get_all_users(session)
        session.close()
        headers = {"access-control-expose-headers": "X-Total-Count", "X-Total-Count": str(users["total"])}
        return JSONResponse(status_code = 200, content = users["data"], headers = headers)
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")


@app.get("/emails")
async def get_all_emails(current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        emails = updatedb.get_all_emails(session)
        session.close()

        total = str(emails["total"])
        emails = emails["data"]
        for email in emails:
            email["recipients"] = [crypto(r, "decode") for r in email["recipients"].split(',')[:-1]]
            email["subject"] = crypto(email["subject"], "decode")
            email["body"] = crypto(email["body"], "decode")
            try:
                email["status_text"] = crypto(email["status_text"], "decode")
            except:
                email["status_text"] = "Null"
        headers = {"access-control-expose-headers": "X-Total-Count", "X-Total-Count": total}
        return JSONResponse(status_code = 200, content = list(reversed(emails)), headers = headers)
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")



@app.post("/adduser")
async def add_user(login: str = Form(...), password: str = Form(...), mail_from: str = Form(''), name_mail_from: str = Form(''), current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        user = updatedb.add_user(session, login = login, password = password, mail_from = mail_from, name_mail_from = name_mail_from)
        session.close()
        if type(user) != str:
            return JSONResponse(status_code = 200, content = f"Пользователь {login} успешно зарегестрирован!")
        else:
            return JSONResponse(status_code = 400, content = user)
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")



@app.post("/del_user")
async def del_user(login: str = Form(...), current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        ans = updatedb.del_user(session, login = login)
        session.close()
        if ans:
            return JSONResponse(status_code = 200, content = f"Пользователь {login} удалён")
        else:
            return JSONResponse(status_code = 404, content = f"Пользователь {login} не найден")
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")



@app.post("/add_mail_from")
async def add_mail_from(mail_from: str = Form(...), name_mail_from: str = Form(''), current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        ans = updatedb.add_mail_from(session, mail_from = mail_from, name_mail_from = name_mail_from)
        session.close()
        return JSONResponse(status_code = 200, content = f"Добавлен отправитель {ans.mail_from}, id = {ans.id}")
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")



@app.post("/change_mail_from")
async def change_mail_from(login: str = Form(...), mail_from: str = Form(...), name_mail_from: str = Form(''), current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        ans = updatedb.change_mail_from(session, login = login, mail_from = mail_from, name_mail_from = name_mail_from)
        session.close()
        if type(ans) != str:
            return JSONResponse(status_code = 200, content = f"Отправитель для пользователя {login} успешно изменен!")
        else:
            return JSONResponse(status_code = 404, content = ans)
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")



@app.post("/change_name_mail_from")
async def change_name_mail_from(mail_from: str = Form(...), new_name_mail_from: str = Form(...), current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        ans = updatedb.change_name_mail_from(session, mail_from = mail_from, new_name_mail_from = new_name_mail_from)
        session.close()
        if type(ans) != str:
            return JSONResponse(status_code = 200, content = f"Имя для отправителя {mail_from} успешно именено на {new_name_mail_from}")
        else:
            return JSONResponse(status_code = 404, content = ans)
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")

 

@app.post("/del_mail_from")
async def del_mail_from(mail_from: str = Form(...), current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        ans = updatedb.del_mail_from(session, mail_from = mail_from)
        session.close()
        if ans != 1:
            return JSONResponse(status_code = 200, content = "Удален отправитель %s. Для пользователей %s отправитель изменен на %s" % (mail_from, ans["usrs"], ans["default"]))
        else:
            return JSONResponse(status_code = 200, content = f"Удален отправитель {mail_from}")
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")



@app.post("/disable")
async def disable(login: str, current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        ans = updatedb.disable(session, login = login)
        session.close()
        if ans:
            return JSONResponse(status_code = 200, content = f"User {login} disabled")
        else:
            return JSONResponse(status_code = 404, content = f"Пользователь {login} не найден")
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")



@app.post("/enable")
async def enable(login: str, current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    if updatedb.is_admin(session, current_user.username):
        ans = updatedb.enable(session, login = login)
        session.close()
        if ans:
            return JSONResponse(status_code = 200, content = f"User {login} enabled")
        else:
            return JSONResponse(status_code = 404, content = f"Пользователь {login} не найден")
    else:
        return JSONResponse(status_code = 403, content = "Только для пользователей из группы администраторов")

#-----------------------------------------------------------------------------------------------------------------------------------

@app.post("/change_password")
async def change_password(new_password: str = Form(...), current_user: schemas.User = Depends(auth.get_current_active_user)):
    session = SessionLocal()
    user = updatedb.change_password(session, id = current_user.id, new_password = new_password)
    session.close()
    if type(user) != str:
        return JSONResponse(status_code = 200, content = f"Пароль для пользователя {current_user.username} успешно изменен!")
    else:
        return JSONResponse(status_code = 400, content = user)



app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

@app.get("/unsubscribe_me")
async def unsubscribe_email(email: str, request: Request):
    return templates.TemplateResponse("unsubscribe.html", {"request": request})



@app.post("/unsubscribe")
async def unsubscribe(email: str):
    try:
        email = crypto(email, "decode")
    except Exception as exc:
        logger.exception(exc)
    unsubscribers = Path() / "unsubscribers"
    with open(unsubscribers / "unsubscribers.txt", "a") as f:
        f.write(email + ";")
    return JSONResponse(status_code = 200, content = f"{email}, вы отписались от рассылки")



@app.post("/subscribe")
async def subscribe(email: str):
    unsubscribers = Path() / "unsubscribers"
    with open(unsubscribers / "unsubscribers.txt", "r") as f:
        file = f.read()
    file = file.split(";")
    res = []
    for f in file:
        if f != email:
            res.append(f)
    with open(unsubscribers / "unsubscribers.txt", "w") as f:
        for r in res:
            f.write(r + ";")
    return JSONResponse(status_code = 200, content = f"{email}, вы снова подписаны на рассылку!")

