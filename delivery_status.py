import requests
#import redis
import time
import os
import re


def find_err(line):
    ind = line.find("SMTP error from remote mail server after")
    ind += 40
    while line[ind] != ':':
        ind += 1
    return ind + 1


def main(recipients, msg_id, unsubscribers):
    recipients = list(reversed(recipients))
    '''r = redis.StrictRedis(
        host = 'redis-17449.c250.eu-central-1-1.ec2.cloud.redislabs.com',
        port = 17449,
        password = 'yKjzHfYXnENAdm9TjMflkabxUySz93Lb',
        charset = "utf-8",
        decode_responses = True
    )
    time.sleep(3)
    res = r.get("file")
    logFileData = []
    for line in list(reversed(res.split('\n'))):
        logFileData.append(line)'''
    resp = requests.get('http://smtp:5002/logs')
    logFileData = []
    for line in list(reversed(resp.content.decode().split('\n'))):
        logFileData.append(line)

    smtp_response = []
    for r in recipients:
        for line in logFileData:
            if re.search(r, line, re.IGNORECASE):
                if '=>' in line:
                    smtp_response.append(line.split('C=')[-1].strip('"'))
                elif '->' in line:
                    smtp_response.append(line.split('C=')[-1].strip('"'))
                elif '==' in line:
                    ind = find_err(line)
                    smtp_response.append(line[ind:].strip(' '))
                elif '**' in line:
                    ind = find_err(line)
                    smtp_response.append(line[ind:].strip(' '))
                break
    res = {}
    for i in range(len(recipients)):
        try:
            status_code = int(smtp_response[i][:3])
            if (status_code >= 200) and (status_code < 400):
                status = "Отправлено"
            elif (status_code >= 400) and (status_code < 500):
                status = "Временная ошибка, будет выполнена ещё одна попытка отправки."
            elif (status_code >= 500):
                status = "Не отправлено"
            res[recipients[i]] = {"status": status, "status_code": status_code, "message": smtp_response[i]}
        except Exception as exc:
            #return exc
            res[recipients[i]] = {"status": smtp_response[i], "status_code": smtp_response[i], "message": smtp_response[i]}
    res_dict = {"response": [{"email": key, "status": res[key]["status"], "status_code": res[key]["status_code"], "message": res[key]["message"]} for key in res.keys()]} #res
    for u in unsubscribers:
        res_dict["response"].append({"email": u, "status": "Не отправлено", "status_code": "-", "message": "Пользователь отписался от рассылки"})
    return res_dict


if __name__ == '__main__':
    recipients = ["nik-rostov@mail.ru","Kit.siNiikit@yandex.ru","vunogradowW@gmail.com"]
    #recipients = ["vunogradoww@gmail.com"]
    res = main(recipients)
    for r in res:
        print(r, res[r], '\n')













'''from fastapi import BackgroundTasks
import requests
import time
import os
import re
#import redis

def check_one_more_time(code_451, rcp_451):
    time.slep(1800)
    resp = requests.get('http://smtp:5002/logs')
    logFileData = []
    for line in list(reversed(resp.content.decode().split('\n'))):
        logFileData.append(line)
    for line in logFileData:
        if re.search(code_451, line):
            for r in recipients:
                if re.search(r, line, re.IGNORECASE):



def find_err(line):
    ind = line.find("SMTP error from remote mail server after")
    ind += 40
    while line[ind] != ':':
        ind += 1
    return ind + 1


def find_code(line):
    line = line.split('==')
    return line[0].split(' ')[2]


def main(recipients, msg_id, unsubscribers, background_tasks: BackgroundTasks):
    recipients = list(reversed(recipients))
    """r = redis.StrictRedis(
        host = 'redis-17449.c250.eu-central-1-1.ec2.cloud.redislabs.com',
        port = 17449,
        password = 'yKjzHfYXnENAdm9TjMflkabxUySz93Lb',
        charset = "utf-8",
        decode_responses = True
    )
    time.sleep(3)
    res = r.get("file")
    logFileData = []
    for line in list(reversed(res.split('\n'))):
        logFileData.append(line)"""
    resp = requests.get('http://smtp:5002/logs')
    logFileData = []
    for line in list(reversed(resp.content.decode().split('\n'))):
        logFileData.append(line)

    smtp_response = []
    rcp_451 = []
    for r in recipients:
        for line in logFileData:
            if re.search(r, line, re.IGNORECASE):
                if '=>' in line:
                    smtp_response.append(line.split('C=')[-1].strip('"'))
                elif '->' in line:
                    smtp_response.append(line.split('C=')[-1].strip('"'))
                elif '==' in line:
                    ind = find_err(line)
                    smtp_response.append(line[ind:].strip(' '))
                    code_451 = find_code(line)
                    rcp_451.apend(r)
                elif '**' in line:
                    ind = find_err(line)
                    smtp_response.append(line[ind:].strip(' '))
                break
    res = {}
    for i in range(len(recipients)):
        try:
            status_code = int(smtp_response[i][:3])
            if (status_code >= 200) and (status_code < 400):
                status = "Отправлено"
            elif (status_code >= 400) and (status_code < 500):
                status = "Временная ошибка, будет выполнена ещё одна попытка отправки."
                background_tasks.add_task(check_one_more_time, codes_451, rcp_451)
            elif (status_code >= 500):
                status = "Не отправлено"
            res[recipients[i]] = {"status": status, "status_code": status_code, "message": smtp_response[i]}
        except Exception as exc:
            #return exc
            res[recipients[i]] = {"status": smtp_response[i], "status_code": smtp_response[i], "message": smtp_response[i]}
    res_dict = {"response": [{"email": key, "status": res[key]["status"], "status_code": res[key]["status_code"], "message": res[key]["message"]} for key in res.keys()]} #res
    for u in unsubscribers:
        res_dict["response"].append({"email": u, "status": "Не отправлено", "status_code": "-", "message": "Пользователь отписался от рассылки"})
    return res_dict


if __name__ == '__main__':
    recipients = ["nik-rostov@mail.ru","Kit.siNiikit@yandex.ru","vunogradowW@gmail.com"]
    #recipients = ["vunogradoww@gmail.com"]
    res = main(recipients)
    for r in res:
        print(r, res[r], '\n')'''



