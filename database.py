from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from pydantic import BaseSettings
import os
import MySQLdb

class Settings(BaseSettings):
    user_name = os.environ["USER_NAME"]
    password = os.environ["PASSWORD"]
    host = os.environ["HOST"]
    database_name = os.environ["DATABASE_NAME"]
    
settings = Settings()

'''class Settings(BaseSettings):
    user_name: str
    password: str
    host: str
    database_name: str
    class Config:
        env_file = ".env"
        env_file_encoding = 'utf-8'
        orm_mode = True
settings = Settings(_env_file='dotenv.env', _env_file_encoding='utf-8')'''


DATABASE = 'mysql://%s:%s@%s/%s?charset=utf8' % (
    settings.user_name,
    settings.password,
    settings.host,
    settings.database_name,
)


def create_db():
    db = MySQLdb.connect(host = settings.host, port = 3306, user = settings.user_name, password = settings.password)
    c = db.cursor()
    c.execute(f'create database if not exists {settings.database_name}')
    db.close()


ENGINE = create_engine(
    DATABASE,
    encoding="utf-8",
    pool_pre_ping=True
)

SessionLocal = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=ENGINE, expire_on_commit = False))
Base = declarative_base()


if __name__ == '__main__':
    create_db()